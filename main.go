package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func router(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadFile("html/" + r.Host + ".html")
	if err != nil {
		http.Redirect(w, r, "http://"+r.Host, http.StatusSeeOther)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	http.ServeContent(w, r, r.URL.Path, time.Now(), bytes.NewReader(data))
}

func main() {
	r := mux.NewRouter()
	r.StrictSlash(false)

	http.HandleFunc("/landing/", router)
	log.Fatal(http.ListenAndServe(":80", nil))
}
